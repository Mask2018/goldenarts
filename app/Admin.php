<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'name',
        'address',
        'postcode',
        'city',
        'phone',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

     public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function hasRole($role){
        return null !== $this->roles()->whereIn('name',$role)->first();
    }

    public function postcodes()
    {
        return $this->belongsToMany('App\Postcode');
    }

    public function hasPostcodes($postcode){
        //return null !== $this->roles()->whereIn('name',$role)->first();
    }
}
