<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
     protected $fillable = [
        'postcode',
        'area',
        'city',
        'status',
    ];

    
    public function admins()
    {
        return $this->belongsToMany('App\Admin');
    }

   
}
