<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficeJob extends Model
{
     protected $fillable = [
         'user_id',
         'title',
         'client',
         'description',
         'start',
         'due',
         'status',
         'designer',
         'notes', 
    ];

   
}
