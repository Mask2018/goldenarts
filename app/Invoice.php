<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'user_id',
		'price',
		'discount',
		'total_price',
		'postcode',
		'Place',
		'paid',
		'shiping_id',
		'status',
		'notes',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
    public function shipping()
    {
    	return $this->belongsTo('App\Shipping', 'shiping_id', 'id');
    }
    public function invoice_detail()
    {
    	return $this->hasMany('App\InvoiceDetail', 'invoice_id');
    }
}
