<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use DB;
use App\Events\NewOrder;
use DataTables;
use App\Category;
use Mailgun\Mailgun;
use App\OfficeJob;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arrayName = array('user' => "Usman");
         broadcast(new NewOrder($arrayName));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);
        return view('admin_panel.orders.invoice',compact('invoice'));
    }

    public function print($id)
    {
      
        $invoice = Invoice::find($id);
     
        return view('admin_panel.orders.print_invoice',compact('invoice'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function invoice_ajax()
    {

        $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC');
                return DataTables::eloquent($model)
                ->addColumn('name', function (Invoice $invoice) {
                    return $invoice->user->full_name;
                })
                ->addColumn('shipping', function (Invoice $invoice) {
                    return $invoice->shipping->type;
                })
               ->addColumn('id', '<a href="invoice/{{$model->id}}">{{$model->id}}</a>')
              
                ->addColumn('action', function (Invoice $invoice) {
                    if ($invoice->status == "Placed") {
                        return '<a href="http://thehalalbutchery.com/thehalalbutchery/public/invoice_update/'.$invoice->id.'/Processing'.'" class="btn" style="background:white; border-radius:50px;width:40px" ><i class="menu-icon mdi mdi-thumb-up" alt="Accept"></i></a>'.'<a href="http://thehalalbutchery.com/thehalalbutchery/public/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>';
                    }
                    else if ($invoice->status == "Processing") {
                        return '<a href="http://thehalalbutchery.com/thehalalbutchery/public/invoice_update/'.$invoice->id.'/Dispatched'.'" class="btn btn-primary" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-truck"></i></a>'.'<a href="http://thehalalbutchery.com/thehalalbutchery/public/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>';
                    }
                     else if($invoice->status == "Dispatched") {
                        return '<a href="http://thehalalbutchery.com/thehalalbutchery/public/invoice_update/'.$invoice->id.'/Deliverd'.'" class="btn btn-success" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-shopping"></i></a>'.'<a href="http://thehalalbutchery.com/thehalalbutchery/public/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>';
                    }
                    
                })
                ->rawColumns(['action','id'])
                ->toJson();


        //return datatables()->of(Invoice::latest()->get())->toJson();
    }

    public function officejob_ajax()
    {
         $model = OfficeJob::latest()->orderBy('id', 'DESC');
                return DataTables::eloquent($model)
                ->addColumn('description', function (OfficeJob $invoice) {
                        return '<textarea style="width:300px;resize: auto;line-height: 20px;" id='.$invoice->id.' onchange="updateDesc(this.value,'.$invoice->id.')">'.$invoice->description.'</textarea>';
                    
                })
              
                ->addColumn('action', function (OfficeJob $invoice) {
                    if ($invoice->status == "new") {
                        return '<a href="http://goldenarts.co.uk/goldenarts/public/job_update/'.$invoice->id.'/Working'.'" class="btn" style="background:white; border-radius:50px;width:40px" ><i class="menu-icon mdi mdi-thumb-up" alt="Accept"></i></a>';
                    }
                    else if ($invoice->status == "Working") {
                        return '<a href="http://goldenarts.co.uk/goldenarts/public/job_update/'.$invoice->id.'/Done'.'" class="btn btn-primary" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-comment-processing"></i></a>';
                    }
                     else if($invoice->status == "Done") {
                        return '<a href="http://goldenarts.co.uk/goldenarts/public/job_update/'.$invoice->id.'/Deliverd'.'" class="btn btn-success" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-shopping"></i></a>';
                    }
                    
                })
                ->rawColumns(['action','description'])
                ->toJson();
        
        //return datatables()->of( OfficeJob::latest()->get())->toJson();
    }



    public function job_update($id, $status)
    {
       // dd($status);
        DB::table('office_jobs')
            ->where('id', $id)
            ->update(['status' => $status]);
            return back();
    }

    public function update_description(Request $r)
    {
        DB::table('office_jobs')
            ->where('id', $r->id)
            ->update(['description' => $r->description]);
           
    }

    public function new_job()
    {
         return view('admin_panel.dashboard.newJob');
    }
    public function save_newjob(Request $r)
    {
         $invoice_detail = OfficeJob::create(['user_id'=>1,'title' => $r->title, 'client' => $r->client, 'description' => $r->description, 'start' => $r->start, 'due'=>$r->due, 'status'=> 'new', 'designer'=>$r->designer, 'notes'=> 'none']);
            return redirect()->route('admin.dashboard');

    }

    public function mail($id,$email)
    {
        
        $invoice = Invoice::find($id);
       
        $contents = view('admin_panel.orders.invoice',compact('invoice'))->render();
      
        $mgClient = new Mailgun('96d0cf4a62498560114ed4a2431ace81-e687bab4-fdd6c128');
        $domain = "sandboxf368ed1e21474f439f82e8f26e8fd414.mailgun.org";
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Order Confermation',
            'html'  => $contents
        ));
        return $result;
    }

    public function invoice_update($id, $status)
    {
       // dd($status);
        DB::table('invoices')
            ->where('id', $id)
            ->update(['status' => $status]);
            return back();
    }
    public function Checkout(){
        $cat = Category::all();
        $url = "https://test.oppwa.com/v1/checkouts";
    $data = "entityId=8ac7a4c978831d9a0178878f1ab60b04" .
                "&amount=92.00" .
                "&currency=GBP" .
                "&paymentType=DB";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                   'Authorization:Bearer OGFjN2E0Y2E3ODgzMjgzZDAxNzg4NzhmMTgyZTBhYTJ8ckY3OTZienB4SA=='));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);
    if(curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);
    //return $responseData;
         //dd(json_decode($responseData));
    $id = json_decode($responseData)->id;
    //dd($id);
        return view('store.checkout',compact('responseData','id', 'cat'));
    }
    public function Checkout_response($id)
    {
        $url = "https://test.oppwa.com/v1/checkouts/$id/payment";
    $url .= "?entityId=8ac7a4c978831d9a0178878f1ab60b04";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                   'Authorization:Bearer OGFjN2E0Y2E3ODgzMjgzZDAxNzg4NzhmMTgyZTBhYTJ8ckY3OTZienB4SA=='));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);
    if(curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);
    return redirect()->route('user.cart');
    //return $responseData;
    //dd($responseData);
    }

    public function apiCheck()
    {
          $add = request()->server('SERVER_ADDR');
          echo $add;
        $url ="https://connect.bluetree-staging.co.uk/v1/apiuser/generatejwttoken";
         $data_array =  array(
            "WebUserID"=> "6f1ee1e2-20f4-404b-a7db-5b0d8c0c004e",
            "ApplicationID"=> "cc127330-bb72-45c3-9e11-912e782963ad",
            "SecretKey"=> "0eb7dc76cacb37ddd096ff4867b74c488d43cfce65d620c17b"   

            // "WebUserID"=> "6f1ee1e2-20f4-404b-a7db-5b0d8c0c004e",
            // "ApplicationID"=> "cc127330-bb72-45c3-9e11-912e782963ad",
            // "SecretKey"=> "13f08cfda83c095dbbc6971fd4203606b3a98fcba4d1a5fcce"   
        );
         $headers = array(
                "X-Custom-Header: value",
                "Content-Type: application/json",
            );

         //dd(json_encode($data_array));
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);        
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data_array) );
        curl_setopt($curl, CURLOPT_FAILONERROR, 1);

        
        $response = curl_exec($curl);
         if($response == FALSE) {
        $errorText = curl_error($curl);
        curl_close($curl);
        die($errorText);
         }


        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
       dd($response);
        return $response;
        
       
    }


}
