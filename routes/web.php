<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Dashboard
//login
Route::get('admin', 'loginController@adminIndex')->name('admin.login');
Route::post('admin', 'loginController@adminPosted');

Route::group(['middleware' => 'admin'], function(){

 
    Route::get("/admin_panel", 'admin_panel\dashboardController@index')->name('admin.dashboard');

    Route::get('admin/logout', 'loginController@adminLogout')->name('admin.logout');
    //categories
    Route::get('/admin_panel/categories', 'admin_panel\categoriesController@index')->name('admin.categories');
    Route::post('/admin_panel/categories', 'admin_panel\categoriesController@posted');

    Route::get('/admin_panel/categories/edit/{id}', 'admin_panel\categoriesController@edit')->name('admin.categories.edit');
    Route::post('/admin_panel/categories/edit/{id}', 'admin_panel\categoriesController@update');

    Route::get('/admin_panel/categories/delete/{id}', 'admin_panel\categoriesController@delete')->name('admin.categories.delete');
    Route::post('/admin_panel/categories/delete/{id}', 'admin_panel\categoriesController@destroy');

    //Questions
    Route::get('/admin_panel/question', 'admin_panel\QuestionController@index')->name('admin.question');
    Route::post('/admin_panel/question/create', 'admin_panel\QuestionController@store')->name('admin.question.store');

    Route::get('/admin_panel/question/edit/{id}', 'admin_panel\QuestionController@edit')->name('admin.question.edit');
    Route::post('/admin_panel/question/update/{id}', 'admin_panel\QuestionController@update')->name('admin.question.update');

    Route::get('/admin_panel/question/delete/{id}', 'admin_panel\QuestionController@delete')->name('admin.question.delete');
    Route::post('/admin_panel/question/delete/{id}', 'admin_panel\QuestionController@destroy');

    //products
    Route::get('/admin_panel/products', 'admin_panel\productsController@index')->name('admin.products');

    Route::get('/admin_panel/products/create', 'admin_panel\productsController@create')->name('admin.products.create');
    
    Route::post('/admin_panel/products/create', 'admin_panel\productsController@store');

    Route::get('/admin_panel/products/edit/{id}', 'admin_panel\productsController@edit')->name('admin.products.edit');
    Route::post('/admin_panel/products/edit/{id}', 'admin_panel\productsController@update');

    Route::get('/admin_panel/products/delete/{id}', 'admin_panel\productsController@delete')->name('admin.products.delete');
    Route::post('/admin_panel/products/delete/{id}', 'admin_panel\productsController@destroy');

    //order management 
    Route::get('/admin_panel/management', 'admin_panel\managementController@manage')->name('admin.orderManagement');
    Route::post('/admin_panel/management', 'admin_panel\managementController@update')->name('admin.orderUpdate');

    //User Managment
    Route::get('/admin_panel/adminManagement', 'admin_panel\AdminController@index')->name('admin.adminManagement');
    Route::get('/admin_panel/adminCreate', 'admin_panel\AdminController@create')->name('admin.adminCreate');
    Route::get('/admin_panel/adminEdit/{id}', 'admin_panel\AdminController@edit')->name('admin.adminEdit');
    Route::Post('/admin_panel/adminStore', 'admin_panel\AdminController@store')->name('admin.adminStore');
    Route::Post('/admin_panel/adminUpdate/{id}', 'admin_panel\AdminController@update')->name('admin.adminUpdate');

    //PostCode Managment
    Route::get('PostcodeManagement', 'PostcodeController@index')->name('Postcode.PostcodeManagement');
    Route::get('PostcodeCreate', 'PostcodeController@create')->name('Postcode.PostcodeCreate');
    Route::get('PostcodeEdit/{id}', 'PostcodeController@edit')->name('Postcode.PostcodeEdit');
    Route::Post('PostcodeStore', 'PostcodeController@store')->name('Postcode.PostcodeStore');
    Route::Post('PostcodeUpdate/{id}', 'PostcodeController@update')->name('Postcode.postcodeUpdate');


});
//Invoice
Route::get('/invoice/{id}', 'InvoiceController@show')->name('invoice.view');
Route::get('/invoice', 'InvoiceController@index')->name('invoice.index');
Route::get('/invoice_ajax', 'InvoiceController@invoice_ajax')->name('invoice.invoice_ajax');
Route::get('/invoice_update/{id}/{status}', 'InvoiceController@invoice_update')->name('invoice.invoice_update');
Route::get('/invoice_print/{id}', 'InvoiceController@print')->name('invoice.print');
Route::get('/mail/{id}', 'InvoiceController@mail')->name('mail');

Route::get('/product/{id}', 'admin_panel\productsController@view')->name('product.view');
Route::get('/login', 'loginController@userIndex')->name('user.login');
Route::post('/login', 'loginController@userPosted');

//signup
Route::get('/signup', 'signupController@userIndex')->name('user.signup');
Route::post('/signup', 'signupController@userPosted');
Route::post('/check_email', 'signupController@emailCheck')->name('user.signup.check_email');


//user
Route::get('/', 'user\userController@index')->name('user.home');

Route::get('/search', 'user\userController@search')->name('user.search');
Route::get('/search?c={id}', 'user\userController@view')->name('user.search.cat');



Route::get('/view/{id}', 'user\userController@view')->name('user.view');
Route::post('/view/{id}', 'user\userController@post')->name('user.postcart');

Route::get('/cart', 'user\userController@cart')->name('user.cart');
Route::post('/cart', 'user\userController@confirm');

Route::post('/edit_cart', 'user\userController@editCart')->name('user.editCart');
Route::post('/delete_item_from_cart', 'user\userController@deleteCartItem')->name('user.deleteCartItem');


Route::get('/logout', 'loginController@userLogout')->name('user.logout');

Route::group(['middleware' => 'user'], function(){
Route::get('/history', 'user\userController@history')->name('user.history');
});

//Checkout
Route::get('/checkout', 'InvoiceController@Checkout')->name('checkout');
Route::get('/Checkout_response/{id}', 'InvoiceController@Checkout_response')->name('Checkout_response');
//test
Route::get("/test", function(){
   return View::make("test");
});
Route::get("/test_pusher", function(){
    event(new App\Events\NewOrder('Welcome'));
   return "Event has Been Sent";
});

// API Routes
Route::get('/apiCheck', 'InvoiceController@apiCheck')->name('apiCheck');

//OfficeJob
Route::get('/officejob_ajax', 'InvoiceController@officejob_ajax')->name('invoice.officejob_ajax');
Route::get('/new_job', 'InvoiceController@new_job')->name('new_job');
Route::post('/save_newjob', 'InvoiceController@save_newjob')->name('save_newjob');
Route::get('/job_update/{id}/{status}', 'InvoiceController@job_update')->name('invoice.job_update');
Route::POST('/update_description', 'InvoiceController@update_description')->name('update_description');