@extends('store.storeLayout')
@section('content')
<!-- SECTION -->
<div class="section">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <th>Order Id</th>
                        <th>Price</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        @foreach($invoice as $s)
                                <tr>
                                <td>{{$s->id}}</td>
                                <td>{{$s->total_price}}</td>
                                <td>{{$s->status}}</td>
                                </tr>
                          
                        @endforeach
                        </tbody>
                </table>
            </div>
        </div>
        <!-- /Billing Details -->
    </div>

</div>

@endsection
