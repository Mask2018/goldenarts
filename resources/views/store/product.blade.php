@extends('store.storeLayout')
@section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>


<style>
    body .inProductHide{padding: 0px !important;}
    .container{max-width: 85%;min-width: 85%;}
    .w-100{max-width: 100%;}
    
    a,*{ text-decoration: none !important;}
    .data_slider_icon a{color: #864837e6;}
    .data_slider_icon{
        position: absolute;
        top: 0px;
        display: block;
        right: -80px;
        width: 65px;
    }
    .data_slider_icon a span{ background-color: red}
    .carousel-inner{height: inherit;}
    .images_slider{width: 85%;height: 370px;}
    .card-body{text-align: center;padding:15px 10px;}
    .bg_grey{background-color: #EEE;}
    .piv_image{width: 100%;height: auto;}
    .slider_indicators {
        padding-left: 0;
        list-style: none;
        margin: 0px;
    }
    .slider_indicators li {
        width: auto;
        margin-bottom:10px;
        padding-bottom: 2px;
        cursor: pointer;
        border-bottom: 2px solid white;
    }
    .delivery_icon span i{font-size: 2rem;}
    .checked { color: #864837e6;}
    .slider_indicators .img_active{border-bottom: 2px solid #821d00;}
    .slider_indicators li:hover{border-bottom: 2px solid #821d00;}
    .slider_indicators .w-100:last-child li{margin-bottom: 0px;}
    .carousel-inner img{width: 100%;object-fit: cover;}
    .carousel-item{height: inherit;}
    .hidden{display: none;}
    a:hover{color:#821d00 !important;}
    .guide_box,.templates_box{
        margin-bottom: 0px;
        padding-top:10px;
        padding-bottom: 10px;
        border-bottom: 1px solid #821d00;
        color: #864837e6;
        cursor: pointer;
        font-size: 16px;
        font-weight: 500;
    }
    .card-body span{color:#333; background: #efedee;font-size: 20px;font-weight: 400;}
    i.fa-truck{color: #864837e6;}
    .border_bottom: {border-bottom: 1px solid lightgrey;}
    .guide_box i,.templates_box i{float: right; color: #864837e6;}
    .guide_show,.templates_show,.guide_box,.templates_box{ padding: 10px 15px; }
    .product_selling_points{  text-align: center; }
    .product_selling_points i{color:#864837e6; }
    ul.classic li{ list-style-type: square;color:#864837e6 }    
    #details_btn,#technical_btn{padding: 10px 40px; cursor: pointer;}
    .details_block,.technical_block{background-color:#efedee;padding: 50px 10px;}
    .btn_action{color:#864837e6;}
    .btn_border{border:1px solid lightgrey; }

    #set_quantity{border:1px solid #ced4da;padding: 4px;float: left;text-align: center;border-radius: 0px;outline: none;}

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    /* Firefox */
    input[type=number] {
      -moz-appearance: textfield;
    }
    .counter.minus,.counter.plus{
        float: left;
        background-color: #864837e6;
        color:white;
        width: 50px;
        border-color:transparent;
        padding: 3px;
        outline: none;
        font-weight: bold;
    }
    .counter.active,.counter:hover{border:2px solid #821d00;}
    .counter.plus{
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }
    .mainColor{
    	color: #864837e6;
    }
    .counter.minus{
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }
    .addToCard{
    	font-size: 22px;
    	text-align: center;
    	background-color: #821d00;
    }
    .btn-primary{background-color: #864837e6 !important;border-color: #864837e6!important;}
    .toggle.btn{height: 32px!important;border-radius: 20px;}
    .toggle-off.btn,.toggle-on.btn{ padding-top:5px!important; }
    .requestQuote{color:#864837e6!important; font-weight: bold;text-decoration: underline;}

    .radio_block{width: 380px;display: flex; margin: 0 auto;}
    .select_radio_options{width:-webkit-fill-available;height: 100px;border:1px solid lightgrey;border-bottom:0px;margin-right: 10px;cursor: pointer;padding: 10px;
        background:linear-gradient(180deg,rgba(255,255,255,.90239846) 30%,#ccc 100%);}
    .select_radio_options span{display: block;font-size: large;}
    .select_radio_options input{width: 40px;height: 25px; cursor: pointer;}
    .change_bg{background:#864837e6;border-color:#864837e6;}

    .express_change_bg{background:#6ecd12!important;}
    .radio_options_selected{background-color: #864837e6;color: white !important;}
    .radio_options_selected span{display: inherit;font-size: medium;}
    .radio_options_selected span strong{font-weight: bold;}
    .fastesticon{font-size: revert!important;font-weight: bold}
    .express_option{margin-right:0px;border: 1px solid #6ecd12; height: 115px; background: linear-gradient(70deg,#55c300 0,#cef35a 100%); }
    .express_option:hover{ background: linear-gradient(70deg,#55c300 0,#cef35a 100%);}
    .express_block{background-color:#6ecd12;color: white; }
    .express_option i{font-size: x-small;}

    .font-size-larger{font-size: larger;}
    .product_table_block{
        background-color: #f4f4f4;
        overflow-y: auto;
        overflow-x: hidden;
        margin-bottom: 10px;
    }
    .product_table_block table thead tr th{
        font-weight: 500;
        font-size: medium;
        box-sizing: border-box;
        padding-top: 18px;
        padding-bottom: 18px;
        border:0px;
    }
    .product_table_block table thead tr th:last-child{padding-top: 4px;padding-bottom: 4px;
        background: linear-gradient(70deg,#55c300 0,#cef35a 100%);}
    .product_table_block table tbody tr td{padding: 15px 5px; background-color: #fff;border:2px solid #f4f4f4; }
    .product_table_block table tbody tr td:first-child{background-color: transparent;font-weight: 500;}

    .product_table_block table thead tr th i{  font-size: smaller;   }
    .product_table_block table tbody tr td:nth-child(2):hover{background-color: #864837e6;border-color:#864837e6;color:white; cursor: pointer;}
    .product_table_block table tbody tr td:nth-child(3):hover{background-color: #864837e6;border-color:#864837e6;color:white; cursor: pointer;}
    .product_table_block table tbody tr td:last-child:hover{background-color: #a1b949;border-color:#a1b949;color:white; cursor: pointer;}
    .product_table_block{display: none;}
    </style>
<!-- SECTION -->

    <!-- container -->
    <div class="container mt-3">
        <div class="row">
            <div class="col-md-6" style="border-right:1px solid lightgrey;">
                <div class="col-12 mb-3" style="border:1px solid lightgrey;">
                    <div class="w-100 text-center pt-3 pb-3">
                        <a href="#" style="color: #333!important;">
                        <span class="font-weight-bold">Excellent</span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span> 1,492 reviews on </span>
                        <span class="fa fa-star checked"></span>
                        <span class="font-weight-bold">Trustpilot</span>
                        </a>
                    </div>
                    <hr class="mt-1 mb-1" style="border-bottom:1px solid lightgrey">
                    <div class="w-100 text-center pt-3 pb-3 delivery_icon">
                        <a href="#" style="color: #333!important;">
                            <span class="w-100 font-weight-bold">
                                <i class="fa fa-truck" aria-hidden="true"></i> Mainland UK Delivery Included
                            </span>
                        </a>
                    </div>
                </div>
                <div id="demo" class="carousel slide images_slider" data-ride="carousel" style="position: relative;">
                    <!-- Indicators -->
                  
                  <!-- The slideshow -->
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="https://www.tradeprint.co.uk/dam/jcr:bbd2776b-907f-4cd4-b30a-fddaef6ae407/comp_Letterhead_3v2-800x800.webp" alt="Los Angeles">
                    </div>
                    <div class="carousel-item">
                      <img src="https://www.tradeprint.co.uk/dam/jcr:c83371cc-8a94-48ea-9ef3-67ba9dddb1d5/comp_pvc_banner_170622_0020-800x800.jpg" alt="Chicago">
                    </div>
                    <div class="carousel-item">
                      <img src="https://www.tradeprint.co.uk/dam/jcr:bbd2776b-907f-4cd4-b30a-fddaef6ae407/comp_Letterhead_3v2-800x800.webp" alt="New York">
                    </div>
                  </div>
                        <!-- Left and right controls -->
                    <div class="data_slider_icon">
                      <div class="w-100 text-center mt-3 mb-2">
                          <a class="" href="#demo" data-slide="next">
                            <i class="fa fa-chevron-up"></i>
                          </a>
                      </div>
                      <ul class="slider_indicators">
                        <div class="w-100">
                            <li data-target="#demo" data-slide-to="0" class="img_active">
                             <img src="https://www.tradeprint.co.uk/dam/jcr:bbd2776b-907f-4cd4-b30a-fddaef6ae407/comp_Letterhead_3v2-800x800.webp" alt="A4 130gsm Silk Flyers" class="piv_image">
                            </li>
                        </div>
                        <div class="w-100">
                           <li data-target="#demo" data-slide-to="1" class="">
                             <img src="https://www.tradeprint.co.uk/dam/jcr:c83371cc-8a94-48ea-9ef3-67ba9dddb1d5/comp_pvc_banner_170622_0020-800x800.jpg" alt="A4 130gsm Silk Flyers" class="piv_image">
                            </li>
                        </div>
                        <div class="w-100">
                           <li data-target="#demo" data-slide-to="2" class="">
                             <img src="https://www.tradeprint.co.uk/dam/jcr:bbd2776b-907f-4cd4-b30a-fddaef6ae407/comp_Letterhead_3v2-800x800.webp" alt="A4 130gsm Silk Flyers" class="piv_image">
                            </li>
                        </div>
                      </ul>
                      <div class="w-100 text-center mt-2">
                          <a class="" href="#demo" data-slide="prev">
                            <i class="fa fa-chevron-down"></i>
                          </a>
                      </div>
                    </div>
                </div>
                <div class="col-12 mt-3 pl-0">
                    <p class="mb-0 border_bottom">
                    You don’t have to be a marketing whizz to know full colour Flyer advertising is still a great promotional tool. Whatever you want to communicate, our marketing Flyers come in a huge range of paper stocks, sizes and finishes so no matter what you want to get across we’ve got you covered!
                    </p>
                </div>
                <div class="w-100">
                  <p class="guide_box">Artwork Guide 
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                  </p>
                  <div class="guide_show hidden">Once you have placed your order you can upload your print ready artwork. Please allow 3mm bleed on all sides, and supply as a CMYK PDF with all fonts outlined or embedded and with a resolution of 300dpi. Download our templates below to assist with setting up your artwork.</div>
                </div>
                <div class="w-100">
                  <p class="templates_box">Artwork Templates 
                    <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                  </p>
                  <div class="templates_show hidden">
                    <img src="#" alt="img">
                  </div>
                </div>
            </div>

            <div class="col-md-6 pl-0 pl-lg-5">
                <div class="col-12 p-0">
                    <h3>Classic Flyers</h3>
                </div>
                <div class="form-group row">
                    <label for="Material" class="col-md-4 col-form-label">Material</label>
                    <div class="col-md-8">
                      <select class="form-control" id="exampleFormControlSelect1">
                          <option>130gsm Gloss</option>
                          <option>100gsm Premium Uncoated</option>
                          <option>400gsm Uncoated</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Size" class="col-md-4 col-form-label">Size</label>
                    <div class="col-md-8">
                      <select class="form-control" id="exampleFormControlSelect1">
                          <option>A4</option>
                          <option>A5</option>
                          <option>Square 105mm x 105mm</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Printed" class="col-md-4 col-form-label">Sides Printed</label>
                    <div class="col-md-8">
                      <select class="form-control" id="exampleFormControlSelect1">
                          <option>Double Sided</option>
                          <option>Single Sided</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Material" class="col-md-4 col-form-label">Lamination</label>
                    <div class="col-md-8">
                      <select class="form-control" id="exampleFormControlSelect1">
                          <option>None</option>
                          <option>Both Sides (Gloss)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Material" class="col-md-4 col-form-label">Kinds</label>
                    <div class="col-md-8">
                        <button type="button" class="counter minus">-</button>
                        <input type="number" id="set_quantity" />
                        <button type="button" class="counter plus">+</button>
                        
                    </div>
                </div>
                <div class="form-group row quantity_block">
                    <label for="Material" class="col-md-4 col-form-label">Quantity</label>
                    <div class="col-md-8">
                      	<select class="form-control" id="exampleFormControlSelect1">
                          <option>500</option>
                          <option>1000</option>
                        </select>
                    </div>
                </div>
                <div class="w-100 form-group text-right">

                  <span class="">Pricing Grid View</span>
                  <input id="toggle-demo" type="checkbox" data-toggle="toggle">
                </div>
                <div class="form-group pt-3 pb-3" style="background-color: #eee">

                    <p class="mb-0 w-100 text-center">Looking for something else? <span class="requestQuote">Request A Quote </span></p>
                </div>

                <div class="w-100 product_table_block">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th class="text-center">Quantity</th>
                                <th class="text-center">Saver</th>
                                <th class="text-center">Standard</th>
                                <th class="text-center">
                                    <i class="mr-1 fa fa-google-wallet" aria-hidden="true"></i>FASTEST <br>Express</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td>10</td>
                                <td>£12.60</td>
                                <td>£14.60</td>
                                <td>£15.60</td>
                            </tr>
                            <tr>
                                <td>20</td>
                                <td>£13.60</td>
                                <td>£15.60</td>
                                <td>£1660</td>
                            </tr>
                            <tr>
                                <td>30</td>
                                <td>£18.60</td>
                                <td>£19.60</td>
                                <td>£22.60</td>
                            </tr>            
                        </tbody>
                    </table>
                </div>


                <div class="w-100 mt-2 mb-2 text-center radio_block_toggle">
                    <div class="radio_block">
                        <div class="select_radio_options mt-4 save_option">
                        	<input type="radio" id="Save" name="option" class="mt-1">
                            <span>Save</span>
                            <strong>-£4.91</strong>
                        </div>
                        <div class="select_radio_options mt-4 standard_option">
                        	<input type="radio" id="Standard" name="option" class="mt-1">
                            <span>Standard</span>
                            <strong>-£49.91</strong>
                        </div>
                        <div class="select_radio_options express_option express_change_bg">
                        	<div class="w-100 text-left"><i class="ml-3 mr-1 mt-2 float-left fa fa-google-wallet" aria-hidden="true"></i>
                    		 <span class="fastesticon">FASTEST</span>
                    		</div>
                        	<input type="radio" id="Express" name="option" class="mt-0" checked>
                            <span>Express</span>
                            <strong>-£4.91</strong>
                        </div>
                    </div>
                    <div class="w-100 p-3 radio_options_selected save_block hidden">
                        <span>Receive by</span>
                        <span><strong>Mon. 17th May.</strong></span>
                    </div>
                    <div class="w-100 p-3 radio_options_selected standard_block hidden">
                        <span>Receive by</span>
                        <span><storng>Thur. 13th May.</storng></span>
                    </div>
                    <div class="w-100 p-3 radio_options_selected express_block">
                        <span>Receive by</span>
                        <span><strong>Wed. 12th May.</strong></span>
                    </div>
                </div>


                <div class="col-md-12 pt-2" style="border:1px solid #eee">
                	<div class="row">
                		<div class="col-md-4">
	                	 <label class="float-left" style="font-weight:500">Artwork Service</label>
	                	</div>
	                	<div class="col-md-8 text-right">
	                	 <span class=""><a href="#" class="mainColor">Which artwork service is right for you?</a></span>
	                	</div>
                	</div>
	                <div class="row p-4" style="background-color:#EEE;">
	                	<select class="form-control" id="exampleFormControlSelect1">
	                        <option>Just Print -FREE</option>
	                        <option>File Check -£6.50</option>
	                    </select>
	                	<p class="w-100 mt-4">We’ll double check your print ready files before printing. Accepted files: PDF, EPS, TIFF, JPEG.</p>
	                </div>
            	</div>
            	<div class="col-md-12 mt-2 mb-2 pt-3 pb-3" style="background-color:#EEE;">
            		<div class="row font-larger">
	                	<div class="col-6">
	                		<input type="checkbox" name="fileCopies" class="mt-0 mr-2 float-left" style="width: 20px; height:18px;">
	                		<span class="font-weight-500">File Copies</span>
	                	</div>
	                	<div class="col-6 text-right">
	                		<span>£4.00</span>
	                	</div>
	                </div>
	            </div>
	            <div class="w-100 pt-3 pb-3 d-inline-block" style="font-size: x-large;color:#5d5d5d;">

	            	<span>TOTAL</span> <span class="float-right">£48.16</span>
	            </div>
	            <div class="w-100">

	            	<button class="addToCard w-100 p-2" style="color:white;border:0px;"> ADD TO BASKET</button>
	            </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">

        <h4 class="text-center p-3">Recommended by Tradeprint</h4>
    </div>

    <div class="container mt-3">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                  <img class="card-img-top" src="https://www.tradeprint.co.uk/dam/jcr:bbd2776b-907f-4cd4-b30a-fddaef6ae407/comp_Letterhead_3v2-800x800.webp" alt="Card image cap">
                  <div class="card-body bg_grey">
                    <span class="card-title ">Letterhead Printing</span>
                  </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                  <img class="card-img-top" src="https://www.tradeprint.co.uk/dam/jcr:4671a41e-dbb7-49d1-bf79-9c95c40245d9/Fomex_with-holes_700px.webp" alt="Card image cap">
                  <div class="card-body bg_grey">
                    <span class="card-title ">Foamex Boards</span>
                  </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                  <img class="card-img-top" src="https://www.tradeprint.co.uk/dam/jcr:d5113ee5-a659-4206-a492-9f367a08d511/Dibond_1_700.webp" alt="Card image cap">
                  <div class="card-body bg_grey">
                    <span class="card-title ">Di-Bond Boards</span>
                  </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                  <img class="card-img-top" src="https://www.tradeprint.co.uk/dam/jcr:c83371cc-8a94-48ea-9ef3-67ba9dddb1d5/comp_pvc_banner_170622_0020-800x800.jpg" alt="Card image cap">
                  <div class="card-body bg_grey">
                    <span class="card-title ">PVC Banners</span>
                  </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5 mb-5">
     <div class="row">
        <div class="col-sm-6 col-md-4 col-lg-3 product_selling_points">
            <i class="fa fa-usd fa-3x" aria-hidden="true"></i>
            <h5 class="mt-2">Competitive Prices</h5>
            <p>Hard-to-beat pricing with trade buyers in mind.</p>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 product_selling_points">
            <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
            <h5 class="mt-2">UK Customer Support</h5>
            <p>Print experts available from 8am to 6pm every weekday.</p>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 product_selling_points">
            <i class="fa fa-cube fa-3x" aria-hidden="true"></i>
            <h5 class="mt-2">White Label Packaging</h5>
            <p>Deliver direct to your clients with peace of mind.</p>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3 product_selling_points">
            <i class="fa fa-trophy fa-3x" aria-hidden="true"></i>
            <h5 class="mt-2">Outstanding Quality</h5>
            <p>We pride ourselves on the quality of our products.</p>
        </div>

     </div>
    </div>

    <div class="container">
        <div class="row">
            <a id="details_btn" class="bg_grey mr-2">Detail</a>
            <a id="technical_btn" class="btn_border">Technical Spec</a>
        </div>
        <div class="row details_block">
            <div class="col-lg-4 text-center">
                <img src="https://www.tradeprint.co.uk/dam/jcr:4671a41e-dbb7-49d1-bf79-9c95c40245d9/Fomex_with-holes_700px.webp" class="w-75">
            </div>
            <div class="col-lg-8">
                <h4>Advertise without breaking the bank</h4>
                <p>Marketing Flyers are the ultimate print essential. No matter what your budget, full colour Flyers will attract your customer from the first instance. Whether you’re posting it through the door or sending out a direct mail, if you get your design right it can be one of the most efficient and cost-effective tools in your marketing arsenal.</p>
                <p>Full colour Flyers are great for raising awareness and they’re a reliable choice. We use only the highest quality materials and modern printing techniques to ensure that while we create an affordable Flyer, you won’t sacrifice on appearance.</p>
                <p>We offer Flyers in anything from a pocket friendly A7 right up to A3 and DL square options too. Fussy about paper? No problem! They’re available in eight paper stocks, from an economic 130gsm right up to a sturdy 450gsm.</p>
                <p>Why not add lamination for a heavy weight finish? Choose the 450gsm Silk with either Gloss, Matt or Soft Touch lamination to create a flyer with striking clarity on reassuringly thick paper.</p>
                <p>Need it fast? Same day printing is available on 130gsm and 170gsm Gloss and Silk, 300gsm Coated and 400gsm Silk Flyers! Select the Same Day option, visible on weekdays before 11am, and we’ll print your Flyers that day and have them on a next day courier service.</p>
                <p>UK shipping is free, exceptions apply.</p>
                <p>All Flyers sized A4 – A7 on paper less than 300gsm and stocks other than uncoated are, tax exempt!</p>
            </div>
        </div>
        <div class="row technical_block hidden">
            <div class="col-lg-4 text-center">
                <img src="https://www.tradeprint.co.uk/dam/jcr:4671a41e-dbb7-49d1-bf79-9c95c40245d9/Fomex_with-holes_700px.webp" class="w-75">
            </div>
            <div class="col-lg-8">
                <h4>Technical Specifications for Classic Flyers</h4>
                <ul class="classic">
                    <li>Include 3mm bleed on all sides</li>
                    <li>Supply as a CMYK PDF</li>
                    <li>Outline or embed fonts</li>
                    <li>300dpi resolution</li>
                </ul>
                <p>If you'd like to order multiple designs of this product, please select the number of different designs required in 'sets'. e.g If you wish to order 2 designs, select 2 sets and a quantity of 1.</p>
                <p>To submit artwork for a sets order, please either supply 1 multi-page PDF with the order running front, back, front, back etc, or submit multiple 2 page PDFs, each containing a front and back for each set.</p>
                <p>For more guidance on how to successfully set up and submit your artwork, see our technical guide.</p>
            </div>
        </div>
    </div>
    <!-- /container -->

<!--JQUERY Validation-->
    <script type="text/javascript">

        $(".slider_indicators li").click(function(){
            // alert('hellos');
            $(".slider_indicators li").removeClass("img_active");
            $(this).addClass('img_active');
        });
        $(".guide_box").click(function(){
            if($(".guide_show").hasClass("hidden")){
                $(".guide_show").removeClass("hidden");
                $(".guide_box i").removeClass("fa-arrow-circle-right");
                $(".guide_box i").addClass("fa-arrow-circle-down");
            }
            else{
                $(".guide_show").addClass("hidden");
                $(".guide_box i").addClass("fa-arrow-circle-right");
                $(".guide_box i").removeClass("fa-arrow-circle-down");
            }
        });
        $(".templates_box").click(function(){
            if($(".templates_show").hasClass("hidden")){
                $(".templates_show").removeClass("hidden");
                $(".templates_box i").removeClass("fa-arrow-circle-right");
                $(".templates_box i").addClass("fa-arrow-circle-down");
            }
            else{
                $(".templates_show").addClass("hidden");
                $(".templates_box i").addClass("fa-arrow-circle-right");
                $(".templates_box i").removeClass("fa-arrow-circle-down");
            }
        });

        $("#details_btn").click(function(){
            $("#details_btn").addClass("bg_grey btn_action").removeClass("btn_border");
            $("#technical_btn").removeClass("bg_grey btn_action").addClass("btn_border");
            $("#details_btn").addClass("bg_grey btn_action");

            $(".details_block").removeClass("hidden");
            $(".technical_block").addClass("hidden");
        });
        $("#technical_btn").click(function(){
            $("#technical_btn").addClass("bg_grey btn_action").removeClass("btn_border");
            $("#details_btn").removeClass("bg_grey btn_action").addClass("btn_border");

            $(".details_block").addClass("hidden");
            $(".technical_block").removeClass("hidden");
        });

        $(".select_radio_options").click(function(){
        	$(".select_radio_options").removeClass("change_bg");
        	$('input',this).prop("checked", true);

        	if( $(this).hasClass("save_option")){
        		$(this).addClass("change_bg");
        		$(".save_block").removeClass("hidden");
        	 	$(".standard_block").addClass("hidden");
        	 	$(".express_block").addClass("hidden");
        	 	$(".express_option").removeClass("express_change_bg");
        	}
        	else if( $(this).hasClass("standard_option")){
        		$(this).addClass("change_bg");
        	 	$(".save_block").addClass("hidden");
        	 	$(".standard_block").removeClass("hidden");
        	 	$(".express_block").addClass("hidden");
        	 	$(".express_option").removeClass("express_change_bg");	
        	}
        	else if( $(this).hasClass("express_option")){
           		$(this).addClass("express_change_bg");
        	 	$(".save_block").addClass("hidden");
        	 	$(".standard_block").addClass("hidden");
        	 	$(".express_block").removeClass("hidden");	
        	}
              	
        });

        /*table hide and show*/
        $('#toggle-demo').change(function(){
            $(".product_table_block").toggle();
            $(".radio_block_toggle").toggle();
            $(".quantity_block").toggle();
        });
        	
    </script>

    <script type="text/javascript">
        var $input = $("#set_quantity");
        // Colocar a 0 ao início
        $input.val(1);
        // Aumenta ou diminui o valor sendo 0 o mais baixo possível
        $(".counter").click(function(){
            if ($(this).hasClass('plus'))
                $input.val(parseInt($input.val())+1);
            else if ($input.val()>1)
                $input.val(parseInt($input.val())-1);
        });
    </script>
    <script>
        $(document).ready(function() {
            $("#set_quantity").on("focus", function() {
                $(this).on("keydown", function(event) {
                    if (event.keyCode === 38 || event.keyCode === 40) {
                        event.preventDefault();
                    }
                });
            });
        });
    </script>
<!--/JQUERY Validation-->
<!-- /SECTION -->
@endsection
