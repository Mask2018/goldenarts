<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Golden Arts</title>

    <link rel="shortcut icon" href="{{asset('img/GA-Fav.png')}}" />
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />


    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/style2.css')}}" />
    
    <!-- JQuery and Validator Plugins -->
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    {{-- custom css --}}
    <style>
        @media only screen and (max-width: 767px){
            #head_links {
                visibility: hidden;
            }
            .custom_search_top {
                text-align:center;
            }

            .header-ctn {
                width: 100%;
            }
        }
        .img-size{
            max-height: 275px;
            min-height: 275px; 
        }
        .carousel-item h3{
            color: white;
        }
         .carousel-inner img {
            width: 100%;
            height: 100%;
        }
        .container{
            min-width: 100%;
        }
        .shop:before {
            width: 45%;
        }
        .carousel-item img{
            border-bottom: 1px solid #821d00;
            border-top: 1px solid #821d00;
            border-left: 1px solid #821d00;
            border-right: 1px solid #821d00;

        }
        .product-preview img{
            border: 3px solid #D10024;
        }
        .quantity
        {
            height: 35px;
            width: 45px;
            border-radius: 10px;
            text-align: center;
            font-weight: bold;
            background-color: white;
            color:white;
            font-size: 15px;

        }
        .quantity:focus{
            background-color: #821d00;
        }
        .product{
        	border-radius: 10px;
            border: 1px solid #821d00; 
        }
        .product-label{
        	border-radius: 10px;
        } 
        .product-body{
        	border-radius: 10px;
        } 
        .add-to-cart{
        	border-radius: 10px;
        }
        .img-size
        {
        	border-top-left-radius: 10px;
    		border-top-right-radius: 10px;
        }
        .section .container .row{
        	    
        	    text-align: center;
        }
        .active{
        	color: red;
        	color: #821d00;
        	font-weight: bold;
        }
        button
        {
        	background-color: #821d00;
        }
        #navigation {
        	border-top: 1px solid #821d00;
            background: #e0e0e0;
        }
        .product .add-to-cart .add-to-cart-btn {
            background-color: #821d00;
        }
        input{
            border-radius: 50px;
            background: white;
        }
        .badge {
  padding-left: 9px;
  padding-right: 9px;
  -webkit-border-radius: 9px;
  -moz-border-radius: 9px;
  border-radius: 9px;
}

.label-warning[href],
.badge-warning[href] {
  background-color: #c67605;
}
#lblCartCount {
    font-size: 12px;
    background: #961515;
    color: #fff;
    padding: 1px 3px;
    vertical-align: top;
    margin-left: -5px; 
}
.steps{
    background: black;
    border: 2px solid #821d00;
}
.steps img{
    display: block;
    margin-left: auto;
    margin-right: auto;
}
#header{
        padding-top: 0px;
    padding-bottom: 0px;
}
.header-ctn {
    padding: 0px 0px;
}
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 9999;
  border-bottom: 2px solid #821d00;
}

.carousel-caption{
    top:140px !important;
    left:8%;
    bottom: initial !important;
    text-align: left;
    width: 400px;

    /*animation-name: example;
  	animation-duration: 5s;*/
}
.carousel-caption.caption_right{
    left: initial;
    right: 6%;
}

.carousel-caption.fadeInLeft {
    -webkit-animation-name: fadeInLeft;
    -moz-animation-name: fadeInLeft;
    -o-animation-name: fadeInLeft;
    animation-name: fadeInLeft;
    -webkit-animation-fill-mode: both;
    -moz-animation-fill-mode: both;
    -o-animation-fill-mode: both;
    animation-fill-mode: both;
    -webkit-animation-duration: 1s;
    -moz-animation-duration: 1s;
    -o-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-delay: 1s;
    -moz-animation-delay: 1s;
    -o-animation-duration:1s;
    animation-delay: 1s;
}   
    @-webkit-keyframes fadeInLeft {
        from {
            opacity:0;
            -webkit-transform: translatex(-10px);
            -moz-transform: translatex(-10px);
            -o-transform: translatex(-10px);
            transform: translatex(-10px);
        }
        to {
            opacity:1;
            -webkit-transform: translatex(0);
            -moz-transform: translatex(0);
            -o-transform: translatex(0);
            transform: translatex(0);
        }
    }
    @-moz-keyframes fadeInLeft {
        from {
            opacity:0;
            -webkit-transform: translatex(-10px);
            -moz-transform: translatex(-10px);
            -o-transform: translatex(-10px);
            transform: translatex(-10px);
        }
        to {
            opacity:1;
            -webkit-transform: translatex(0);
            -moz-transform: translatex(0);
            -o-transform: translatex(0);
            transform: translatex(0);
        }
    }
    @keyframes fadeInLeft {
        from {
            opacity:0;
            -webkit-transform: translatex(-100px);
            -moz-transform: translatex(-100px);
            -o-transform: translatex(-100px);
            transform: translatex(-100px);
        }
        to {
            opacity:1;
            -webkit-transform: translatex(0);
            -moz-transform: translatex(0);
            -o-transform: translatex(0);
            transform: translatex(0);
        }
    }


.carousel-caption.fadeInRight {
    -webkit-animation-name: fadeInRight;
    -moz-animation-name: fadeInRight;
    -o-animation-name: fadeInRight;
    animation-name: fadeInRight;
    -webkit-animation-fill-mode: both;
    -moz-animation-fill-mode: both;
    -o-animation-fill-mode: both;
    animation-fill-mode: both;
    -webkit-animation-duration: 1s;
    -moz-animation-duration: 1s;
    -o-animation-duration: 1s;
    animation-duration: 1s;
    -webkit-animation-delay: 1s;
    -moz-animation-delay: 1s;
    -o-animation-duration:1s;
    animation-delay: 1s;
}
            @-webkit-keyframes fadeInRight{
                0%{
                    opacity:0;
                    -webkit-transform:translateX(200px);
                    transform:translateX(200px)
                }
                100%{
                    opacity:1;
                    -webkit-transform:translateX(0);
                    transform:translateX(0)
                }
            }
            @keyframes fadeInRight{
                0%{
                    opacity:0;
                    -webkit-transform:translateX(200px);
                    -ms-transform:translateX(200px);
                    transform:translateX(200px)
                }
                100%{
                    opacity:1;
                    -webkit-transform:translateX(0);
                    -ms-transform:translateX(0);
                    transform:translateX(0)
                }
            }
            



            

.carousel-caption.fadeIn{
   animation: fadeIn 5s;
  -webkit-animation: fadeIn 5s;
  -moz-animation: fadeIn 5s;
  -o-animation: fadeIn 5s;
  -ms-animation: fadeIn 5s;  
}
    @keyframes fadeIn {
      0% {opacity:0;}
      100% {opacity:1;}
    }

    @-moz-keyframes fadeIn {
      0% {opacity:0;}
      100% {opacity:1;}
    }

    @-webkit-keyframes fadeIn {
      0% {opacity:0;}
      100% {opacity:1;}
    }

    @-o-keyframes fadeIn {
      0% {opacity:0;}
      100% {opacity:1;}
    }

    @-ms-keyframes fadeIn {
      0% {opacity:0;}
      100% {opacity:1;}
    }

.carousel-caption h3{
    font-size: 30px; 
}

.carousel-caption p{
	padding-left: 40px;
    font-size: 25px;
    font-family: monospace;
    line-height: 23px;
    width: 400px;
    margin-bottom: 40px;
    color: yellow;
}/* 
@keyframes example {
	from   {color:white; left:20px; top:0px;}
  	to {color:yellow; left:100px; top:0px;}
}*/ 


.carousel_order_now{
    background: #821d00;
    border-radius: 7px;
    color: white;
    border:2px solid;
    padding:7px 45px;
    font-size: large;
    font-weight: 400;
    letter-spacing: 1px;
}
.white_color{color:white;border-color:white;}


.megamenu-li {
	position: static;
}

.megamenu {
	position: absolute;
	width: 100%;
	left: 0;
	right: 0;
	padding: 15px;
}
.megamenu h5{border-bottom: 1px dotted grey;margin-bottom: 2px;padding: 5px 10px;color:#864837e6;}
.second_child_level,.third_child_level{display: none;}
.megamenu .dropdown-item:hover .arrow_right{border-color: #864837e6;}
.dropdown-item{cursor: pointer;}
.arrow_right{
  float: right;
  border: solid lightgrey;
  border-width: 0 2px 2px 0;
  display: inline-block;
  padding: 3px;
  margin-top: 5px;
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
}
</style>

</head>

<body>
    <!-- HEADER -->
    <header>
        <!-- TOP HEADER -->
        <div id="top-header">
            <div class="container" style="min-width: 90%">
                <ul id="head_links" class="header-links pull-left">
                    <li><a href="#"><i class="fa fa-envelope-o" style="color: #821d00"></i> info@goldenarts.co.uk</a></li>
                   
                    <li style="border-left: 1px solid #821d00; padding-left: 10px; "><a href="#"> About Us</a></li>

                    <li style="border-left: 1px solid #821d00; padding-left: 10px; "><a href="#"> Delivery Process</a></li>
                </ul>
                <ul class="header-links pull-right">
                    @if(session()->has('user'))
                      <li><a style="color:white" href="{{route('user.history')}}">{{session()->get('user')->full_name}} </a></li>  
                      <li><a href="{{route('user.logout')}}"><i class="fa fa-user-o" style="color: #821d00"></i> Logout</a></li>
                    @else
                    <li><a href="{{route('user.login')}}"><i class="fa fa-user-o" style="color: #821d00"></i> Login</a></li>
                    
                    <li><a href="{{route('user.signup')}}"><i class="fa fa-user-o" style="color: #821d00"></i> SignUp</a></li>
                    @endif
                    
                </ul>
            </div>
        </div>
        <!-- /TOP HEADER -->

        <!-- MAIN HEADER -->
        <div id="header" style="background-image: url('https://goldenarts.co.uk/goldenarts/public/img/1500x128px2.png');background-repeat: no-repeat; background-attachment: fixed; background-size: contain; height: 80px">
            <!-- container -->
            <div class="container" style="min-width: 90%">
                <!-- row -->
                <div class="row">
                    <!-- LOGO -->
                    <div class="col-md-3">
                        <div class="header-logo">
                            <a href="{{route('user.home')}}" class="logo">
                                <img src="{{asset('img/GA-Logo.png')}}" style="width: 120px; margin-top: 10px" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- /LOGO -->

                    <!-- SEARCH BAR -->
                    <div class="col-md-6" style="margin-top: 10px">
                        <div class="header-search">
                            <form action="{{route('user.search')}}" method="get">
                                <div class="custom_search_top" >
                                    <input class="input" style="border-radius: 40px 0px 0px 40px;" name="n" placeholder="Search here">
                                    <button  class="search-btn" style="background-color: #821d00">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /SEARCH BAR -->

                    <!-- ACCOUNT -->
                    <div class="col-md-3 clearfix"  style="margin-top: 30px">
                        <div class="header-ctn">
                            <!-- Cart -->
                            <div  class="dropdown">
                                <a class="dropdown-toggle " id="custom_shopping_cart" href="{{route('user.cart')}}">
                                    <i class="fa fa-shopping-cart" matBadge="15" style="color: #ffffff"><span class='badge badge-warning' id='lblCartCount'> {{ (Session::get('orderCounter'))?Session::get('orderCounter'):"0"}} </span></i>
                                    <span >Your Cart</span>
                                </a>

                            </div>
                            <!-- /Cart -->

                            <!-- Menu Toogle -->
                            <div class="menu-toggle pull-right" >
                                <a href="#">
                                    <i class="fa fa-bars"></i>
                                    <span>Menu</span>
                                </a>
                            </div>
                            <!-- /Menu Toogle -->
                        </div>
                    </div>
                    <!-- /ACCOUNT -->
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </div>
        <!-- /MAIN HEADER -->
    </header>
    <!-- /HEADER -->

    <!-- NAVIGATION -->
    <nav id="navigation" style="height: 55px">
        <!-- container -->
        <div class="container" style="min-width: 90%">
            <!-- responsive-nav -->
            <div id="responsive-nav">
                <!-- NAV -->
                <ul class="navbar " >
                  
                    <li class="">
				       <a class="{{Route::is('user.home') ? 'active' : ''}}" href="{{route('user.home')}}" >Home</a>
				       
				     </li>

                    @if(Route::is('user.search'))
                        @foreach($cat as $c)
                        <li class="nav-item dropdown megamenu-li"><a href="{{route('user.search.cat',['id'=>$c->id])}}" class=" nav-item {{$c->id == $a ? 'active' : ''}} nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$c->name}}</a>
                        <div class="dropdown-menu megamenu" aria-labelledby="dropdown01">
    				        <div class="row"> 
                                <div class="col-sm-6 col-lg-3">
        				          <a class="dropdown-item" href="#">Standard Business Cards</a>
        				          <a class="dropdown-item" href="#">Folders</a>
        				          <a class="dropdown-item" href="#">NCR</a>
        				          <a class="dropdown-item" href="#">Cards</a>
        				        </div>
                            </div>
				        </div>
                        </li>
                        @endforeach
                        <li ><a href="search" class=" nav-item {{$a == -1  ? 'active' : ''}}">Browse All</a></li>
                    @else
                        @foreach($cat as $c)
                        <li class="nav-item dropdown megamenu-li"><a href="{{route('user.search.cat',['id'=>$c->id])}}" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >{{$c->name}}</a>
                        	<div class="dropdown-menu megamenu" aria-labelledby="dropdown01">
    				            <div class="row">
            				        <div class="col-sm-6 col-lg-3">
            				          <h5>First Links</h5>
            				          <span class="dropdown-item second_level">Standard Business Cards <i class="arrow_right"></i></span>
            				          <span class="dropdown-item">Folders</span>
            				          <span class="dropdown-item">NCR</span>
            				          <span class="dropdown-item">Cards</span>
        				            </div>
                                    <div class="col-sm-6 col-lg-3 second_child_level">
                                      <h5>Second Links</h5>
                                      <span class="dropdown-item">Standard Business Cards</span>
                                      <span class="dropdown-item third_level">Folders<i class="arrow_right"></i></span>
                                      <span class="dropdown-item">NCR</span>
                                      <span class="dropdown-item">Cards</span>
                                    </div>
                                    <div class="col-sm-6 col-lg-3 third_child_level">
                                      <h5>Third Links</h5>
                                      <span class="dropdown-item">Standard Business Cards</span>
                                      <span class="dropdown-item">Folders</span>
                                      <span class="dropdown-item">NCR</span>
                                      <span class="dropdown-item">Cards</span>
                                    </div>
    				            </div>
				            </div>
                        </li>
                        @endforeach
                        <li ><a href="{{route('user.search')}}">Browse All</a></li>
                    @endif
                    
                </ul>
                <!-- /NAV -->
            </div>
            <!-- /responsive-nav -->
        </div>
        <!-- /container -->
    </nav>
    <!-- /NAVIGATION -->

    <!-- SECTION -->
    <div class="alert alert-success" hidden role="alert">
        Your order Succesfully Placed!
    </div>
    <div class="section inProductHide">
        <!-- container -->
        <div class="container" style="padding: 0px;">
            <!-- row -->
            @if(Route::is('user.home'))
            <!-- Slider Stat -->
            <div id="demo" class="carousel slide" data-ride="carousel">
              <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
              </ul>
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <img src="{{asset('images/slides/GA-Website-banners-02.png')}}" alt="Los Angeles" style="width: 100%; height: 500px">
                  <div class="carousel-caption fadeInLeft">
                    <h3 style="color: #fbb62c">Box Printing</h3>
                    <p style="color: white">Golden Arts Provide Best printing Quality!</p>
                    <a href="#" class="white_color carousel_order_now">Order Now</a>
                  </div>   
                </div>
                <div class="carousel-item">
                  <img src="{{asset('images/slides/GA-Website-banners-03.png')}}" alt="Chicago" style="width: 100%; height: 500px">
                  <div class="carousel-caption fadeInRight caption_right">
                    <h3 style="color: #1274ae">Calender</h3>
                    <p style="color: Black">Get Your Brander Calenders!</p>
                    <a href="#" class="carousel_order_now">Order Now</a>
                  </div>   
                </div>
                <div class="carousel-item">
                  <img src="{{asset('images/slides/GA-Website-banners-04.png')}}" alt="New York" style="width: 100%; height: 500px">
                  <div class="carousel-caption fadeInLeft">
                    <h3 style="color: #d1572d">Folded Flyres</h3>
                    <p style="color: #241651">We belive in best Services!</p>
                    <a href="#" class="carousel_order_now">Order Now</a>
                  </div>   
                </div>
              </div>
              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
              </a>
            </div>

            <!-- Slider End -->
            <div class="col-sm-12">
            <div class="row" style="padding: 15px; background: none ">
                <!-- shop -->
                @php
                $counter=0;
                @endphp
                @foreach($cat as $c)
                 @php
                $counter++;
                if($counter==4)
                break;
               
                @endphp
                <div class="col-md-4 col-xs-6">
                    <div class="shop">
                        <div class="shop-img">
                            <img class="img-size" src="./img/shop0{{$index++}}.jpg" alt="">
                        </div>
                        <div class="shop-body">
                            <h3>{{$c->name}}</h3>
                            <a href="search?c={{$c->id}}" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <!-- /shop -->
                @endforeach
            </div>
            </div>
            @endif
            <!-- /row -->


        </div>
        <!-- /container -->
    </div>
    <!-- SECTION -->



    @yield('content')

    <!-- /SECTION -->
    
    <div id="newsletter" class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
           
            <div class="row">
                <div class="col-md-12">
                    <div class="newsletter">
                        <p>Sign Up for the <strong>NEWSLETTER</strong></p>
                        <form>
                            <input class="input" type="email" placeholder="Enter Your Email">
                            <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                        </form>
                        <ul class="newsletter-follow">
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /NEWSLETTER -->

    <!-- FOOTER -->
    <footer id="footer" >
        <!-- top footer -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row" >
                    <div class="col-md-3 col-xs-6" >
                        <div class="footer" >
                            <h3 class="footer-title">About Us</h3>
                            <p>We deal in Fresh meat.</p>
                            <ul class="footer-links">
                                <li><a href="#"><i class="fa fa-map-marker"></i>Chorltan</a></li>
                                <li><a href="#"><i class="fa fa-phone"></i>+44-11-22-33</a></li>
                                <li><a href="#"><i class="fa fa-envelope-o"></i>info@thehalalbutchery.com</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Categories</h3>
                            <ul class="footer-links">
                                <li><a href="#">Hot deals</a></li>
                                <li><a href="#">Chicken</a></li>
                                <li><a href="#">Beef</a></li>
                                <li><a href="#">Mutton</a></li>
                                <li><a href="#">Lamb</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="clearfix visible-xs"></div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Information</h3>
                            <ul class="footer-links">
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Orders and Returns</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-xs-6">
                        <div class="footer">
                            <h3 class="footer-title">Service</h3>
                            <ul class="footer-links">
                                <li><a href="#">My Account</a></li>
                                <li><a href="#">View Cart</a></li>
                                <li><a href="#">Wishlist</a></li>
                                <li><a href="#">Track My Order</a></li>
                                <li><a href="#">Help</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /top footer -->

        <!-- bottom footer -->
        <div id="bottom-footer" class="section">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="footer-payments">
                            <li><a href="#"><i class="fa fa-cc-visa"></i></a></li>
                            <li><a href="#"><i class="fa fa-credit-card"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-paypal"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-mastercard"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-discover"></i></a></li>
                            <li><a href="#"><i class="fa fa-cc-amex"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /bottom footer -->
    </footer>
    <!-- /FOOTER -->


    <!-- jQuery Plugins -->
   <!--  <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="{{asset('js/nouislider.min.js')}}"></script>
    <script src="{{asset('js/jquery.zoom.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/lib/jquery.js')}}"></script>
    <script src="{{asset('js/dist/jquery.validate.js')}}"></script>
    

    <script type="text/javascript">
        window.onscroll = function() {myFunction()};

    var header = document.getElementById("navigation");
    var sticky = header.offsetTop;

    function myFunction() {

      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }

    $(".megamenu").on("click", function(e) {
		e.stopPropagation();
	});

    $(".second_level").click(function(){
        $(".second_child_level").toggle();
        $(".third_child_level").hide();

    });
    $(".third_level").click(function(){
        $(".third_child_level").toggle();

    });

    </script> 
</body> 
</html> 