@extends('admin_panel.adminLayout')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <h4 class="card-title col-md-10">Jobs</h4>
                        <a class="col-md-2 btn-primary" href="{{route('new_job')}}" style="text-align: center;">New Job</a>
                    </div>
                    
                    
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="jobs_table">
                            <thead>
                                <!-- `id`, `user_id`, `price`, `discount`, `total_price`, `postcode`, `Place`, `paid`, `shiping_id`, `notes`, `status` -->
                                <tr>
                                    <th>
                                        Job Id
                                    </th>
                                    <th>
                                       Client
                                    </th>
                                    <th>
                                        DESC
                                    </th>
                                    <th>
                                        Start
                                    </th>
                                    <th>
                                        Due
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Designer
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content_js')
<script type="text/javascript">
    orders();
    function orders(){
        $('#jobs_table').DataTable({
            "order": [[ 1, "DESC" ]],
            "lengthMenu": [[ 50, -1], [ 50, "All"]],
            processing:true,
            serverside:true,
            retrieve: true,
            destroy:true,
            ajax:{
                url:"{{ route('invoice.officejob_ajax')}}",
            },
            columns:[
            {   
                data: 'id',
                name: 'id'
            },

             {
                data: 'client',
                name: 'client'
             },

             {
                data: 'description',
                name: 'description'
             },

             {
                data: 'start',
                name: 'start'
             },

             {
                data: 'due',
                name: 'due'
                
             },
             {
                data: 'status',
                name: 'status'
             },
            {
                data: 'designer',
                name: 'designer'
             },
             {
                data: 'action',
                name: 'action'
             },

            ],
            'rowCallback': function(row, data, index){
            if(data.status == 'new'){
                //console.log(data.status); 
                $(row).css('background-color', 'green');
            }
        }
        });
    }

    function updateDesc(value,id)
    {

            var token=$("input[name=_token]").val();
            $.ajax({
                /* the route pointing to the post function */
                url: "{{route('update_description')}}",
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: token, id: id,description: value},
                dataType: 'JSON',
                /* remind that 'data' is the response of the AjaxController */
                success: function (data) { 
                   
                }
            }); 
           
    }
</script>
@endsection