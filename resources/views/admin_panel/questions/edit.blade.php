@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}</style>



<div class="content-wrapper">
    <div class="row">
        <div class="col-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Options</h4>
                    <form class="forms-sample" method="post" id="question_form" action="{{route('admin.question.update',$id)}}">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Question</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="question" id="question" value="{{$question->question}}" placeholder="Enter New Question">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Options</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="option" id="option" placeholder="Enter New Option">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success mr-2">Submit</button>
                    </form>
                    @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $err)
                        <tr>
                            <td>
                                <li>{{$err}}</li>
                            </td>
                        </tr>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Options Table</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 80%;">
                                        Option
                                    </th>
                                    <th>Actions</th>
                                    
                                   
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($olist as $cat)
                                <tr>
                                    <td>
                                        {{$cat->option}}
                                    </td>
                                    
                                    <td>
                                        <a href="{{route('admin.question.edit', ['id' => $cat->id])}}" class="btn btn-warning">Edit</a>
                                   &nbsp
                                        <a href="{{route('admin.question.delete', ['id' => $cat->id])}}" onclick="delete()" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--JQUERY Validation-->
<script>
  
  $(document).ready(function() {
    
    $("#cat_form").validate({
      rules: {
        Name: "required",
        Type: "required",
        
        
        
      },
      messages: {
        Name: "Category Name is Required",
        Type: "Category Type is Required",
                  
      }
    });

    
  });
  </script>
<!--/JQUERY Validation-->

@endsection
