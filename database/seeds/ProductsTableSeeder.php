<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
            'name' => 'Classic Business Cards',
                'image_name' => '1.jpg',
                'description' => 'Classic Business Cards are our most affordable and most popular choice. With a wide range of paper stocks and styles to choose from, these cost-effective cards are an essential for any business. We offer affordable Business Cards in quantities from 50 – 50,000 units in a wide range of materials and creative finishes.',
                'price' => 2.49,
                'discount' => 2.49,
                'tag' => 'New',
                'category_id' => 1,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Classic Spot UV Business Cards',
                'image_name' => '1.jpg',
                'description' => 'When your client wants attention-grabbing, custom business cards, upgrade to our Spot UV Business Cards. This vibrant visual effect adds extra luxe that is impossible to ignore!',
                'price' => 2.99,
                'discount' => 2.99,
                'tag' => 'New',
                'category_id' => 1,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            2 =>  
            array (
                'id' => 3,
                'name' => 'Saddle Stitch Booklets',
                'image_name' => '1.jpg',
                'description' => 'Saddle Stitch Booklet printing is out most affordable binding option. With low minimum order quantities and custom sizes from A6 up to A4, Saddle Stitch Booklets are the perfect choice for brochures with lower page counts and projects with shorter lead times.',
                'price' => 5.49,
                'discount' => 5.49,
                'tag' => 'New',
                'category_id' => 2,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Perfect Bound Booklets',
                'image_name' => '1.jpg',
                'description' => 'Choose Perfect Bound Book printing for your client’s premium Brochures. Using a PUR adhesive, these Brochures are built to last and can handle heavy paper stocks and a high page volume. Mix and match from a wide range of cover and inner stocks and select custom sizes up to A4!',
                'price' => 11.99,
                'discount' => 11.99,
                'tag' => 'Hot',
                'category_id' => 2,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-10',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Promotional Boxes',
                'image_name' => '1.jpg',
                'description' => 'Promotional Boxes offer a new opportunity to serve clients looking for product packaging. Add their branding to these custom printed boxes and ensure their products arrive in style with professional packaging.',
                'price' => 8.99,
                'discount' => 8.99,
                'tag' => 'Hot',
                'category_id' => 3,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-12',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Best Buy Calendars',
                'image_name' => '1.jpg',
                'description' => 'Let us do the choosing for you! We have taken the hard work out of the equation with our Best Buy Tradeprint recommended Calendars! Offer your clients a Calendar for every end of year promotional opportunity. Lightweight and useful, custom calendars are the perfect choice for giveaways when on the go or sending in the post.',
                'price' => 4.49,
                'discount' => 4.49,
                'tag' => 'Hot',
                'category_id' => 4,
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            
        ));
        
        
    }
}