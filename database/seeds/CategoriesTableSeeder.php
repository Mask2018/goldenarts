<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Cards & Stationery',
                'type' => 'Cards & Stationery',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Boocklets & Brouchers',
                'type' => 'Boocklets & Brouchers',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Labels & Packaging',
                'type' => 'Labels & Packaging',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Campaigns',
                'type' => 'Campaigns',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Aditional Services',
                'type' => 'Aditional Services',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            
        ));
        
        
    }
}